from django.db import models
from django.contrib.auth.models import User


# Create your models here.
class EventType(models.Model):
	typename = models.CharField(max_length=100)

class Org(models.Model):
	org_name = models.CharField(max_length=100)
	org_url = models.SlugField()
	org_icon = models.ImageField()

class Event(models.Model):
	eventType = models.ForeignKey(EventType)
	org = models.ForeignKey(Org)
	name = models.CharField(max_length=255,blank=True)
	description = models.TextField(blank=True)
	has_location = models.BooleanField()
	location_name = models.CharField(max_length=255,blank=True)
	street_address = models.CharField(max_length=255,blank=True)
	city = models.CharField(max_length=255,blank=True)
	zipcode = models.CharField(max_length=5,blank=True)
	lat = models.FloatField(null=True)
	lon = models.FloatField(null=True)
	event_date = models.DateField()
	event_time = models.CharField(max_length=30,blank=True)
	url = models.CharField(max_length=255)
	is_live = models.BooleanField(default=False)
	is_active = models.BooleanField(default=True)
	created_by = models.ForeignKey(User)
	created_at = models.DateTimeField(auto_now_add = True)
	updated_at = models.DateTimeField(auto_now = True)

class Profile(models.Model):
	user = models.OneToOneField(User)
	org = models.ForeignKey(Org)