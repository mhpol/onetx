from django.conf.urls import url
from . import views

urlpatterns = [
	url(r'^$', views.home, name="home"),
	url(r'^admin$', views.admin, name="admin"),
	url(r'^admin/new-event/$', views.new_event, name="admin-new-event"),
	url(r'^admin/edit-event/(?P<event_id>[0-9]+)/$',  views.edit_event, name="admin-edit-event"),
	url(r'^admin/publish/$', views.publish, name="admin-publish"),
	url(r'^admin/active/$', views.active, name="admin-active"),
	url(r'^admin/new-user/$', views.new_user, name="admin-new-user"),
	url(r'^ajax-all-events$', views.ajax_all_events, name="ajax-all-events"),

]