# import urllib.request ## <- for production
import urllib
import json

# def mapajax(request):
#     location = request.GET.get('location')
#     location = location.replace(' ', '+')
#     address = location + ',+Texas'
#     apikey =  'AIzaSyCKjvW-c9UV2u1bkLqe0If6Fh8A5HgcUW4' # MORGAN'S ACCOUNT 
#     gmapsrequesturl = 'https://maps.googleapis.com/maps/api/geocode/json?address=%s&key=%s' % (address, apikey)
#     print(gmapsrequesturl)
#     try:
#         with urllib.request.urlopen(gmapsrequesturl) as response:
#             data = json.load(response)
#             if data['status'] == 'OK':
#                 data = data['results'][0]['geometry']['location']
#             elif data['status'] == "ZERO_RESULTS":
#                 data = {'status': "ZERO_RESULTS"}
#     except IOError:
#         data = {'status':'ERROR'}
#     return data

def geo_api(location):
    location = location.replace(' ', '+')
    address = location + ',+Texas'
    apikey =  'AIzaSyCKjvW-c9UV2u1bkLqe0If6Fh8A5HgcUW4' # MORGAN'S ACCOUNT 
    gmapsrequesturl = 'https://maps.googleapis.com/maps/api/geocode/json?address=%s&key=%s' % (address, apikey)
    print(gmapsrequesturl)
    try:
        with urllib.request.urlopen(gmapsrequesturl) as response:
            str_response = response.read().decode('utf-8')
            data = json.loads(str_response)
            # data = json.load(response)
            if data['status'] == 'OK':
                data = data['results'][0]['geometry']['location']
            elif data['status'] == "ZERO_RESULTS":
                data = {'status': "ZERO_RESULTS"}
    except IOError:
        data = {'status':'ERROR'}
    return data

def is_superuser(user):
    return (True if user.id == 1 else False)

