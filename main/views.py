from django.contrib.auth.decorators import login_required, user_passes_test
from django.shortcuts import render
from .models import EventType,Event,Org
from django.contrib.auth.models import User
from django.urls import reverse
from django.http import HttpResponseRedirect,JsonResponse,HttpResponse
from .functions import geo_api,is_superuser
from django.utils import timezone
import datetime,math

# Create your views here.
def admin(request):

	req_user = User.objects.get(pk=request.user.id)

	user_org_events = Event.objects.filter(org=req_user.profile.org,is_active=True)

	return render(request,'main/admin.html',{'events': user_org_events})

def new_event(request):

	if request.method == 'POST':

		if request.POST['has_location'] == "1":
			location = request.POST.get('street_address','') + ' ' + request.POST.get('city','') + ' ' + request.POST.get('zipcode','')
			loc_result = geo_api(location)

			print('Loc result:')
			print(loc_result)

			if 'lat' in loc_result:
				lat = loc_result['lat']
				lon = loc_result['lng']
			else:
				lat = None
				lon = None

		else:
			lat = None
			lon = None

		raw_ev_date = request.POST.get('event_date')
		event_date = datetime.datetime.strptime(raw_ev_date, '%m/%d/%Y').date()

		ev = Event(
			created_by = User.objects.get(pk=request.user.id),
			eventType = EventType.objects.get(pk=request.POST.get('eventtype')),
			org = Org.objects.get(pk=request.user.profile.org.id),
			name = request.POST.get('name',''),
			description = request.POST.get('description',''),
			has_location = request.POST.get('has_location','1'),
			location_name = request.POST.get('location_name',''),
			street_address = request.POST.get('street_address',''),
			city = request.POST.get('city',''),
			zipcode = request.POST.get('zipcode',''),
			lat = lat,
			lon = lon,
			event_date = event_date,
			event_time = request.POST.get('event_time'),
			url = request.POST.get('url','')
		)

		ev.save()

		r_url = reverse('admin', kwargs={})
		return HttpResponseRedirect(r_url)

	else:
		eventtypes = EventType.objects.all();
		return render(request,'main/new-event.html',{'eventtypes': eventtypes})


@login_required(login_url="/login/")
def edit_event(request,event_id):

	ev = Event.objects.get(pk=event_id)

	if request.method == 'POST':

		if request.POST['has_location'] == "1":
			location = request.POST.get('street_address','') + ' ' + request.POST.get('city','') + ' ' + request.POST.get('zipcode','')
			loc_result = geo_api(location)

			print('Loc result:')
			print(loc_result)

			if loc_result['lat']:
				lat = loc_result['lat']
				lon = loc_result['lng']
			else:
				lat = None
				lon = None

		else:
			lat = None
			lon = None

		raw_ev_date = request.POST.get('event_date')
		event_date = datetime.datetime.strptime(raw_ev_date, '%m/%d/%Y').date()

		ev.eventType = EventType.objects.get(pk=request.POST.get('eventtype'))
		ev.org = Org.objects.get(pk=request.user.profile.org.id)
		ev.name = request.POST.get('name','')
		ev.description = request.POST.get('description','')
		ev.has_location = request.POST.get('has_location','1')
		ev.location_name = request.POST.get('location_name','')
		ev.street_address = request.POST.get('street_address','')
		ev.city = request.POST.get('city','')
		ev.zipcode = request.POST.get('zipcode','')
		ev.lat = lat
		ev.lon = lon
		ev.event_date = event_date
		ev.event_time = request.POST.get('event_time')
		ev.url = request.POST.get('url','')

		ev.save()

		r_url = reverse('admin', kwargs={})
		return HttpResponseRedirect(r_url)

	else:
		eventtypes = EventType.objects.all();
		return render(request,'main/edit-event.html',{'event': ev,'eventtypes': eventtypes})



def home(request):

	if request.method == "POST":

		date_now = timezone.localtime(timezone.now()).date()
		events = Event.objects.filter(is_live=True,is_active=True,event_date__gte = date_now)

		if request.POST['location']:
			loc_result = geo_api(request.POST['location'])

			print('loc_result:')
			print(loc_result)

			## convert to kms - just a hack.
			loc_dist = request.POST.get('distance')

			dist = float(loc_dist)*1.60934 
			mylon = loc_result['lng']
			mylat = loc_result['lat']
			lon1 = mylon-dist/abs(math.cos(math.radians(mylat))*111.0) 
			lon2 = mylon+dist/abs(math.cos(math.radians(mylat))*111.0)
			lat1 = mylat-(dist/111.0)
			lat2 = mylat+(dist/111.0)
			events = events.filter(lat__range=(lat1, lat2)).filter(lon__range=(lon1, lon2))

		if request.POST['from_date']:
			sql_from_date = "event_date >= cast('" + request.POST['from_date'] + "' as date)"
			events = events.extra(where=[sql_from_date]) 

		if request.POST['to_date']:
			sql_to_date = "event_date <= cast('" + request.POST['to_date'] + "' as date)"
			events = events.extra(where=[sql_to_date]) 


		print(events.query);

		print(events)

		events = events.select_related('org__org_name','eventType__typename').order_by('event_date').values(
				'has_location',
				'description',
				'name',
				'zipcode',
				'eventType__typename',
				'lat',
				'lon',
				'location_name',
				'street_address',
				'event_time',
				'city',
				'id',
				'url',
				'event_date',
				'org__org_name')

		resp = {
			'events': list(events)
		}

		return JsonResponse(resp)

	else:
		return render(request,'main/home.html',{})

def ajax_all_events(request):
	## get current date
	date_now = timezone.localtime(timezone.now()).date()
	events = Event.objects.filter(event_date__gte = date_now, is_live=True, is_active=True).select_related('org__org_name','eventType__typename').order_by('event_date').values(
			'has_location',
			'description',
			'name',
			'zipcode',
			'eventType__typename',
			'lat',
			'lon',
			'location_name',
			'street_address',
			'event_time',
			'city',
			'id',
			'url',
			'event_date',
			'org__org_name')

	print(events);

	resp = {
		'events': list(events)
	}

	return JsonResponse(resp)
	
@login_required(login_url="/login/")
def publish(request):
	if request.method == "POST":
		event_id = request.POST.get('event_id')
		status = request.POST.get('status',0)

		ev = Event.objects.get(pk=int(event_id))

		status = (True if status == '1' else False)

		ev.is_live = status
		ev.save()

	return HttpResponse(status=204)

@login_required(login_url="/login/")
def active(request):
	if request.method == "POST":
		event_id = request.POST.get('event_id')
		status = request.POST.get('status',0)

		ev = Event.objects.get(pk=int(event_id))

		status = (True if status == '1' else False)

		ev.is_active = status
		ev.save()

	return HttpResponse(status=204)

@user_passes_test(is_superuser,login_url="/login/",redirect_field_name=None)
@login_required(login_url="/login/")
def new_user(request):
	return render(request,'main/new-user.html')


